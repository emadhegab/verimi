//
//  ProviderEntityTests.swift
//  verimiTest
//
//  Created by Mohamed Hegab on 9/9/19.
//  Copyright © 2019 Mohamed Emad Abdalla Hegab. All rights reserved.
//
import XCTest
@testable import VerimiTest

class ProviderEntityTests: XCTestCase {

    func testProviderSetGet() {
        let provider = Provider(name: "verimi", desc: "short desc")
        XCTAssertEqual(provider.displayName, "verimi")

    }
}
