//
//  ProviderInteractorTests
//
//
//  Created by Mohamed Hegab on 8/24/19.
//  Copyright © 2019 Mohamed Emad Abdalla Hegab. All rights reserved.
//

import XCTest
import MHNetwork
@testable import VerimiTest
class ProviderInteractorTests: XCTestCase {

    let timeout: TimeInterval = 10

    var mockedProviderInteractor: MockedProviderInteractor!
    override func setUp() {
        super.setUp()
        mockedProviderInteractor = MockedProviderInteractor()
    }

    override func tearDown() {
        mockedProviderInteractor = nil
        super.tearDown()
    }

    func testProviderInteractorGetsCorrectResponse() {
        let expectation = self.expectation(description: "Retrieve providers  ")
        mockedProviderInteractor.getProviders(onCompletion: { (providers) in
            defer { expectation.fulfill() }
            XCTAssertNotNil(providers)
        }) { (error) in
            defer { expectation.fulfill() }
            XCTFail("test shouldn't fail!")
        }

        waitForExpectations(timeout: timeout, handler: nil)
    }

    func testProviderInteractorGetsIncorrectResponse() {
        let expectation = self.expectation(description: "Retrieve providers ")
        mockedProviderInteractor.fail = true
        mockedProviderInteractor.getProviders(onCompletion: { (providers) in
            XCTFail("test should fail!")
        }) { (error) in
            defer { expectation.fulfill() }
            XCTAssertEqual(error.code, HTTPStatusCodes.internalServerError)
        }

        waitForExpectations(timeout: timeout, handler: nil)
    }

}


class MockedProviderInteractor: ProviderInteractorProtocol {
    var fail: Bool = false
    var presenter: ProviderPresenterProtocol?

    func getProviders(onCompletion: @escaping ([Provider]) -> Void, onError: @escaping (ErrorItem) -> Void) {

        if fail {
            onError(ErrorItem(code: HTTPStatusCodes.internalServerError, error: nil, data: nil))
            return
        }

        if let path = Bundle.main.url(forResource: "provider", withExtension: "json") {
            let data = try! Data(contentsOf: path, options: .mappedIfSafe)
            let decoder = JSONDecoder()

            do {
            let providers = try decoder.decode([Provider].self, from: data)
                onCompletion(providers)
            } catch {
                onError(ErrorItem(code: HTTPStatusCodes.internalServerError, error: error, data: nil))
            }
        }
    }


}
