//
//  AppDelegate.swift
//  VerimiTest
//
//  Created by Mohamed Emad Abdalla Hegab on 9/4/19.
//  Copyright © 2019 Verimi. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let router = Container.shared.createBaseRouter()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        window = UIWindow(frame: UIScreen.main.bounds)

        router.route()
        return true
    }
}

