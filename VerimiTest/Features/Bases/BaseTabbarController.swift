//
//  BaseTabbarController.swift
//  VerimiTest
//
//  Created by Mohamed Emad Abdalla Hegab on 9/7/19.
//  Copyright © 2019 Verimi. All rights reserved.
//

import UIKit

class BaseTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabs()
    }

    private func setupTabs() {
        let listProviderVC = ProviderRouter.createModule()
        listProviderVC.tabBarItem = UITabBarItem(title: "List", image: #imageLiteral(resourceName: "list-icon"), selectedImage: nil)

        let createProviderVC = ProviderCreateRouter.createModule()
        createProviderVC.tabBarItem = UITabBarItem(title: "Create", image: #imageLiteral(resourceName: "create-icon"), selectedImage: nil)
        viewControllers = [listProviderVC, createProviderVC]
    }
}

