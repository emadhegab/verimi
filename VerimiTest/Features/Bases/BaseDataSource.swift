//
//  BaseDataSource.swift
//  VerimiTest
//
//  Created by Mohamed Emad Abdalla Hegab on 9/7/19.
//  Copyright © 2019 Verimi. All rights reserved.
//
import UIKit

protocol BaseTableDataSource: class, UITableViewDelegate, UITableViewDataSource {
    associatedtype ItemType
    var items: [ItemType] { get set }
}
