//
//  ProviderDetailsPresenter.swift
//  VerimiTest
//
//  Created Mohamed Emad Abdalla Hegab on 9/7/19.
//  Copyright © 2019 Verimi. All rights reserved.
//


import UIKit

class ProviderDetailsPresenter: ProviderDetailsPresenterProtocol {

    weak private var view: ProviderDetailsViewProtocol?
    var interactor: ProviderDetailsInteractorProtocol?
    private let router: ProviderDetailsWireframeProtocol

    init(interface: ProviderDetailsViewProtocol, interactor: ProviderDetailsInteractorProtocol?, router: ProviderDetailsWireframeProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }

}
