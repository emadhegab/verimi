//
//  ProviderDetailsViewController.swift
//  VerimiTest
//
//  Created Mohamed Emad Abdalla Hegab on 9/7/19.
//  Copyright © 2019 Verimi. All rights reserved.
//


import UIKit
import SDWebImage

class ProviderDetailsViewController: UIViewController, ProviderDetailsViewProtocol {

	var presenter: ProviderDetailsPresenterProtocol?
    var provider: Provider!

    @IBOutlet weak var providerImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionTV: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
         setProviderData(provider)
    }

    private func setProviderData(_ provider: Provider) {
        nameLabel.text = provider.displayName
        descriptionTV.text = provider.description
        providerImage.image = #imageLiteral(resourceName: "default")
        if let urlPath = provider.imageUrl {
            providerImage.sd_setImage(with: URL(string: urlPath), completed: nil)
        }
    }
}
