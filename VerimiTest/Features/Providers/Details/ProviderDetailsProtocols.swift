//
//  ProviderDetailsProtocols.swift
//  VerimiTest
//
//  Created Mohamed Emad Abdalla Hegab on 9/7/19.
//  Copyright © 2019 Verimi. All rights reserved.
//


import Foundation

//MARK: Wireframe -
protocol ProviderDetailsWireframeProtocol: class {

}
//MARK: Presenter -
protocol ProviderDetailsPresenterProtocol: class {
    
}

//MARK: Interactor -
protocol ProviderDetailsInteractorProtocol: class {

  var presenter: ProviderDetailsPresenterProtocol?  { get set }
}

//MARK: View -
protocol ProviderDetailsViewProtocol: class {

  var presenter: ProviderDetailsPresenterProtocol?  { get set }
}
