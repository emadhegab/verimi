//
//  ProviderDetailsRouter.swift
//  VerimiTest
//
//  Created Mohamed Emad Abdalla Hegab on 9/7/19.
//  Copyright © 2019 Verimi. All rights reserved.
//


import UIKit

class ProviderDetailsRouter: ProviderDetailsWireframeProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule(provider: Provider) -> UIViewController {
        // Change to get view from storyboard if not using progammatic UI
        let view = ProviderDetailsViewController(nibName: nil, bundle: nil)
        let interactor = ProviderDetailsInteractor()
        let router = ProviderDetailsRouter()
        let presenter = ProviderDetailsPresenter(interface: view, interactor: interactor, router: router)
        view.provider = provider
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        
        return view
    }

    static func route(from: UIViewController, with provider: Provider, animated: Bool) {
        from.navigationController?.pushViewController(createModule(provider: provider), animated: animated)
    }
}
