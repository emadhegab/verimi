//
//  CreateProviderTask.swift
//  VerimiTest
//
//  Created by Mohamed Emad Abdalla Hegab on 9/7/19.
//  Copyright © 2019 Verimi. All rights reserved.
//


import MHNetwork

class CreateProviderTask<T: Codable>: Operations {

    var provider: Provider

    init(provider: Provider) {
        self.provider = provider
    }

    var request: Request {
        return ProviderRequests.createProvider(provider: provider)
    }

    func execute(in dispatcher: Dispatcher, completed: @escaping (T) -> Void, onError: @escaping (ErrorItem) -> Void) {

        do {
            try dispatcher.execute(request: self.request, completion: { response in
                switch response {
                case .data(let data):
                    do {
                        let decoder = JSONDecoder()
                        let object = try decoder.decode(T.self, from: data)
                        completed(object)
                    } catch let error {
                        print("error Parsing with Error: \(error.localizedDescription)")
                    }
                    break
                case .error(let error):
                    onError(error)
                    break
                }
            }, onError: onError)
        } catch {
            print("error")
        }
    }
}

