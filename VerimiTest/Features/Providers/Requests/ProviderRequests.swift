//
//  ProviderRequests.swift
//  VerimiTest
//
//  Created by Mohamed Emad Abdalla Hegab on 9/7/19.
//  Copyright © 2019 Verimi. All rights reserved.
//

import MHNetwork

enum ProviderRequests: Request {
    case getProviders
    case createProvider(provider: Provider)

    var path: String {
        switch self {
        case .getProviders,.createProvider:
            return "rest/service-providers"
        }
    }

    var method: HTTPMethod {
        switch self {
        case .getProviders:
            return .get
        case .createProvider:
            return .post
        }
    }

    var parameters: RequestParams {
        switch self {
        case .getProviders:
            return .url(nil)
        case .createProvider(let provider):
            return .body(["displayName": provider.displayName, "description": provider.description])
        }
    }

    var headers: [String : Any]? {
        return nil
    }
}
