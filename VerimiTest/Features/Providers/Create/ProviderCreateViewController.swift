//
//  ProviderCreateViewController.swift
//  VerimiTest
//
//  Created Mohamed Emad Abdalla Hegab on 9/7/19.
//  Copyright © 2019 Verimi. All rights reserved.
//


import UIKit

class ProviderCreateViewController: UIViewController, ProviderCreateViewProtocol {

	var presenter: ProviderCreatePresenterProtocol?

    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var descriptionTV: UITextView!
    @IBOutlet weak var submitButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func alert(_ provider: Provider) {
        let alertController = UIAlertController(title: "Created", message:
            "Provider \(provider.displayName) Created!", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default))

        self.present(alertController, animated: true, completion: nil)

        reset()
    }

    @IBAction func createProviderclicked(_ sender: UIButton) {
        sender.isEnabled = false
        dismissKeyboard()
        createProvider()
    }

    private func createProvider() {
        if !(nameTF?.text!.isEmpty)! || !descriptionTV.text.isEmpty {
            let provider = Provider(name: nameTF.text!, desc: descriptionTV.text!)
            presenter?.createProvider(provider)
        } else {
            submitButton.isEnabled = true
        }
    }

    private func dismissKeyboard() {
        self.nameTF.resignFirstResponder()
        self.descriptionTV.resignFirstResponder()
    }

    private func reset() {
        nameTF.text = ""
        descriptionTV.text = ""
        submitButton.isEnabled = true
        
    }
}
