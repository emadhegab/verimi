//
//  ProviderCreateInteractor.swift
//  VerimiTest
//
//  Created Mohamed Emad Abdalla Hegab on 9/7/19.
//  Copyright © 2019 Verimi. All rights reserved.
//


import MHNetwork

class ProviderCreateInteractor: ProviderCreateInteractorProtocol {

    weak var presenter: ProviderCreatePresenterProtocol?

    func createProvider(provider: Provider, onCompletion: @escaping (Provider) -> Void, onError: @escaping (ErrorItem) -> Void) {
        let dispatcher = Container.shared.createNetworkDispatcher()
        let createProviderTask = CreateProviderTask<Provider>(provider: provider)
        createProviderTask.execute(in: dispatcher, completed: onCompletion, onError: onError)
    }
}
