//
//  ProviderCreateProtocols.swift
//  VerimiTest
//
//  Created Mohamed Emad Abdalla Hegab on 9/7/19.
//  Copyright © 2019 Verimi. All rights reserved.
//


import MHNetwork

//MARK: Wireframe -
protocol ProviderCreateWireframeProtocol: class {

}
//MARK: Presenter -
protocol ProviderCreatePresenterProtocol: class {
    func createProvider(_ provider: Provider)
}

//MARK: Interactor -
protocol ProviderCreateInteractorProtocol: class {

    var presenter: ProviderCreatePresenterProtocol?  { get set }
    func createProvider(provider: Provider, onCompletion: @escaping (Provider) -> Void, onError: @escaping (ErrorItem) -> Void)
}

//MARK: View -
protocol ProviderCreateViewProtocol: class {

    var presenter: ProviderCreatePresenterProtocol?  { get set }
    func alert(_ provider: Provider)
}
