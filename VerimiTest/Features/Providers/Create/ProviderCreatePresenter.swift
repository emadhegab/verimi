//
//  ProviderCreatePresenter.swift
//  VerimiTest
//
//  Created Mohamed Emad Abdalla Hegab on 9/7/19.
//  Copyright © 2019 Verimi. All rights reserved.
//


import UIKit

class ProviderCreatePresenter: ProviderCreatePresenterProtocol {

    weak private var view: ProviderCreateViewProtocol?
    var interactor: ProviderCreateInteractorProtocol?
    private let router: ProviderCreateWireframeProtocol

    init(interface: ProviderCreateViewProtocol, interactor: ProviderCreateInteractorProtocol?, router: ProviderCreateWireframeProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }

    func createProvider(_ provider: Provider) {
        interactor?.createProvider(provider: provider, onCompletion: {(provider) in
            DispatchQueue.main.async { [weak self] in
                self?.view?.alert(provider)
            }
        }, onError: { (error) in
            print(error)
        })
    }

}
