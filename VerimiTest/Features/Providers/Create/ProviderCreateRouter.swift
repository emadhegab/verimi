//
//  ProviderCreateRouter.swift
//  VerimiTest
//
//  Created Mohamed Emad Abdalla Hegab on 9/7/19.
//  Copyright © 2019 Verimi. All rights reserved.
//


import UIKit

class ProviderCreateRouter: ProviderCreateWireframeProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        // Change to get view from storyboard if not using progammatic UI
        let view = ProviderCreateViewController(nibName: nil, bundle: nil)
        let interactor = ProviderCreateInteractor()
        let router = ProviderCreateRouter()
        let presenter = ProviderCreatePresenter(interface: view, interactor: interactor, router: router)
        
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        
        return view
    }
}
