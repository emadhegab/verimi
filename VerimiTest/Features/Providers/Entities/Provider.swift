//
//  Provider.swift
//  VerimiTest
//
//  Created by Mohamed Emad Abdalla Hegab on 9/7/19.
//  Copyright © 2019 Verimi. All rights reserved.
//

import Foundation

struct Provider: Codable, Equatable {
    let _id: String
    let displayName: String
    let description: String
    let imageUrl: String?

    init(name: String, desc: String) {
        self._id = ""
        self.displayName = name
        self.description = desc
        imageUrl = ""
    }
}
