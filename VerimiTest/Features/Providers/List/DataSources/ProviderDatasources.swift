//
//  ProviderDatasources.swift
//  VerimiTest
//
//  Created by Mohamed Emad Abdalla Hegab on 9/7/19.
//  Copyright © 2019 Verimi. All rights reserved.
//

import UIKit
import SDWebImage

protocol ProviderDataSourceDelegate: class {
    func ProviderSelected(provider: Provider)
}
class ProviderDatasources: NSObject, BaseTableDataSource {

    var items: [Provider] = []
    weak var delegate: ProviderDataSourceDelegate?

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProviderCell.reuseId) as? ProviderCell else { return UITableViewCell(frame: CGRect.zero) }
        let provider = self[indexPath]
        cell.nameLabel.text = provider.displayName
        cell.descriptionLabel.text = provider.description
        cell.providerImage.image = #imageLiteral(resourceName: "default")
        if let urlPath = provider.imageUrl {
            cell.providerImage.sd_setImage(with: URL(string: urlPath), completed: nil)
        }

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let provider = self[indexPath]
        delegate?.ProviderSelected(provider: provider)
    }
}


extension ProviderDatasources {
    subscript(indexPath: IndexPath) -> Provider {
        return items[indexPath.row]
    }
}
