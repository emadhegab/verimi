//
//  ProviderProtocols.swift
//  VerimiTest
//
//  Created Mohamed Emad Abdalla Hegab on 9/7/19.
//  Copyright © 2019 Verimi. All rights reserved.
//


import MHNetwork

//MARK: Wireframe -
protocol ProviderWireframeProtocol: class {

}
//MARK: Presenter -
protocol ProviderPresenterProtocol: class {
    func getProviders()
}

//MARK: Interactor -
protocol ProviderInteractorProtocol: class {

    var presenter: ProviderPresenterProtocol?  { get set }
    func getProviders(onCompletion:  @escaping ([Provider]) -> Void, onError: @escaping  (ErrorItem) -> Void)
}

//MARK: View -
protocol ProviderViewProtocol: class {

    var presenter: ProviderPresenterProtocol?  { get set }
    func reloadData(providers: [Provider])
}
