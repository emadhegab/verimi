//
//  ProviderRouter.swift
//  VerimiTest
//
//  Created Mohamed Emad Abdalla Hegab on 9/7/19.
//  Copyright © 2019 Verimi. All rights reserved.
//


import UIKit

class ProviderRouter: ProviderWireframeProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        // Change to get view from storyboard if not using progammatic UI
        let view = ProvidersViewController(nibName: nil, bundle: nil)
        let interactor = ProviderInteractor()
        let router = ProviderRouter()
        let presenter = ProviderPresenter(interface: view, interactor: interactor, router: router)
        
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        
        return view
    }
}
