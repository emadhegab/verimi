//
//  ProviderInteractor.swift
//  VerimiTest
//
//  Created Mohamed Emad Abdalla Hegab on 9/7/19.
//  Copyright © 2019 Verimi. All rights reserved.
//


import UIKit
import MHNetwork

class ProviderInteractor: ProviderInteractorProtocol {

    weak var presenter: ProviderPresenterProtocol?

    func getProviders(onCompletion: @escaping ([Provider]) -> Void, onError: @escaping (ErrorItem) -> Void) {
        let dispatcher = Container.shared.createNetworkDispatcher()
        let getProvidersTask = GetProviderTask<[Provider]>()
        getProvidersTask.execute(in: dispatcher, completed: onCompletion, onError: onError)
    }
}
