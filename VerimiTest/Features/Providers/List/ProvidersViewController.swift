//
//  ProviderViewController.swift
//  VerimiTest
//
//  Created Mohamed Emad Abdalla Hegab on 9/7/19.
//  Copyright © 2019 Verimi. All rights reserved.
//


import UIKit

class ProvidersViewController: UIViewController, ProviderViewProtocol {

    @IBOutlet weak var tableView: UITableView!
    let requestsTimeIntervals: TimeInterval = 15
	var presenter: ProviderPresenterProtocol?
    var datasources = ProviderDatasources()
    var timer: Timer!

	override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
        getProviders()
        timer = Timer.scheduledTimer(timeInterval: requestsTimeIntervals, target: self, selector: #selector(getProviders), userInfo: nil, repeats: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)


    }

    func reloadData(providers: [Provider]) {
        let oldItems = datasources.items
        let newItems = providers.filter { !oldItems.contains($0) }
        let deletedItems = oldItems.filter { !providers.contains($0) }

        let deletedIndexPaths = deletedItems.map { deleted in
            IndexPath(row: datasources.items.firstIndex(where: {$0 == deleted})!, section: 0)
        }

        if deletedItems.count > 0 {
            datasources.items.remove(objects: deletedItems)

            self.tableView.beginUpdates()
            self.tableView.deleteRows(at: deletedIndexPaths, with: .automatic)
            self.tableView.endUpdates()
        }

        if newItems.count > 0 {
            datasources.items.append(contentsOf: newItems)
            let insertIndexPaths = (datasources.items.count - (newItems.count) ..< datasources.items.count)
            .map { IndexPath(row: $0, section: 0) }

            self.tableView.beginUpdates()
            self.tableView.insertRows(at: insertIndexPaths, with: .automatic)
            self.tableView.endUpdates()
        }
    }

    private func setupTable() {
        tableView.rowHeight =  UITableView.automaticDimension
        datasources.delegate = self
        tableView.dataSource = datasources
        tableView.delegate = datasources
        tableView.register(cell: ProviderCell.self)
        datasources.items = []

    }

   @objc private func getProviders() {
        presenter?.getProviders()
    }

    deinit {
        timer.invalidate()
    }
}

extension ProvidersViewController: ProviderDataSourceDelegate {
    func ProviderSelected(provider: Provider) {
        ProviderDetailsRouter.route(from: self, with: provider, animated: true)
    }


}
