//
//  ProviderPresenter.swift
//  VerimiTest
//
//  Created Mohamed Emad Abdalla Hegab on 9/7/19.
//  Copyright © 2019 Verimi. All rights reserved.
//


import UIKit

class ProviderPresenter: ProviderPresenterProtocol {

    weak private var view: ProviderViewProtocol?
    var interactor: ProviderInteractorProtocol?
    private let router: ProviderWireframeProtocol

    init(interface: ProviderViewProtocol, interactor: ProviderInteractorProtocol?, router: ProviderWireframeProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }

    func getProviders() {
        interactor?.getProviders(onCompletion: { (providers) in
            DispatchQueue.main.async { [weak self]  in
                self?.view?.reloadData(providers: providers)
            }

        }, onError: { (error) in

        })
    }
}
