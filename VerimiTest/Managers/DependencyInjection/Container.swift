//
//  Container.swift
//  VerimitTest
//
//  Created by Mohamed Emad Abdalla Hegab on 07.09.19.
//  Copyright © 2019 Scooters. All rights reserved.
//

import Foundation
import MHNetwork

class Container {

    static let shared = Container()

    func createBaseRouter() -> BaseRouter {
        return DefaultBaseRouter()
    }

    func createNetworkDispatcher() -> NetworkDispatcher {

        let  defaultHeaders = ["Content-Type": "application/json",
                               "Accept": "application/json",
                               "x-apikey": "460888f4c77f2cba6c7fbb5e6be5f6cdf0937"]
        let url: String = "https://verimitest-4446.restdb.io/"

        var environment = Environment(host: url)
        // should create headers here

        environment.headers = defaultHeaders
        let session = URLSession(configuration: URLSessionConfiguration.ephemeral)
        let dispatcher =  NetworkDispatcher(environment: environment, session: session)
        return dispatcher
    }

}
