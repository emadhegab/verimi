//
//  Array+Extension.swift
//  VerimiTest
//
//  Created by Mohamed Emad Abdalla Hegab on 9/8/19.
//  Copyright © 2019 Verimi. All rights reserved.
//

import UIKit

extension Array where Element: Equatable {
    mutating func remove(objects: [Element]) {
        for object in objects {
            guard let index = firstIndex(of: object) else {return}
            remove(at: index)
        }
    }

}
